package Model.DAO;

import java.util.LinkedList;
import java.util.List;

public class Pilha {
    private List<String> lista = new LinkedList<>();
    private String numero = "";
    
    public void inserir(String n){
        this.lista.add(n);
    }
    
    public String remover(){
        return this.lista.remove(this.lista.size()-1);
    }
    
    public boolean vazia(){
        return this.lista.size() == 0;
    }
    
    public void setNumero(){
        for(int i=lista.size()-1; i>=0; i--){
            this.numero = this.numero.concat(lista.get(i).toString());
        }
    }
    
    public String getNumero(){
        this.setNumero();
        return this.numero;
    }
    
    public void imprimir(){
        for(String s:lista){
            System.out.print(s);
        }
    }
    
    public void imprimir2(){
        System.out.print(" [");
        for(int i=lista.size()-1; i>=0; i--){
            System.out.print(lista.get(i));
        }
        System.out.print("] ");
    }
}
