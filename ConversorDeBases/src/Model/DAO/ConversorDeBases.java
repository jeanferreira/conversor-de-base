
package Model.DAO;

import static java.lang.Math.pow;

public class ConversorDeBases {
    
    public static Pilha converter(double numero, int base){
        Pilha lista = new Pilha();
        
        if(numero<base){
            lista.inserir(converterCaractereHexadecimal(numero));//Permite incluir os caracteres especiais do hexadecimal
        } else{
            while(numero >= base){
                lista.inserir(converterCaractereHexadecimal(numero%base));
                //caractereHexadecimal(numero%base, lista);

                if((numero/base)<base)
                    lista.inserir(converterCaractereHexadecimal(numero/base));
                    //lista.inserir(Integer.toString(numero/base));

                numero/=base;
            }
        }
        return lista;
    }
    
    public static double reverter(String n, int base){
        double resultado=0;
        int i, tam = n.length()-1;
        
        for(i=0; i<n.length(); i++, tam--)
            resultado+=reverterCaractereHexadecimal(String.valueOf(n.charAt(i))) * pow(base,tam);
        
        return resultado;
    }
    
    private static String converterCaractereHexadecimal(double n){        
        switch((int)n){
            case 10: return "A";
            case 11: return "B";
            case 12: return "C";
            case 13: return "D";
            case 14: return "E";
            case 15: return "F";
            default: return Integer.toString((int)n);
        }
    }
    
    private static int reverterCaractereHexadecimal(String n){
        switch(n){
            case "A": return 10;
            case "B": return 11;
            case "C": return 12;
            case "D": return 13;
            case "E": return 14;
            case "F": return 15;
            case "a": return 10;
            case "b": return 11;
            case "c": return 12;
            case "d": return 13;
            case "e": return 14;
            case "f": return 15;
            default: return Integer.parseUnsignedInt(n);
        }
    }
}
